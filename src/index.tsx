import {
  EmitterSubscription,
  NativeEventEmitter,
  NativeModules,
  Platform,
} from 'react-native';

const LINKING_ERROR =
  `The package 'react-native-screen-lock-detector' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

const ScreenLockDetector = NativeModules.ScreenLockDetector
  ? NativeModules.ScreenLockDetector
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

const ScreenLockEmitter = new NativeEventEmitter(ScreenLockDetector);

type ScreenLockEvents = 'screenStateChanged';

class ScreenLockService {
  public async getDisplayState() {
    return await ScreenLockDetector.getDisplayState();
  }

  public addListener(
    event: ScreenLockEvents,
    cb: (isScreenOn: boolean) => void
  ) {
    return ScreenLockEmitter.addListener(event, cb);
  }

  public removeSubscription(subscription: EmitterSubscription) {
    ScreenLockEmitter.removeSubscription(subscription);
  }

  public removeAllListeners(event: ScreenLockEvents) {
    ScreenLockEmitter.removeAllListeners(event);
  }
}

export const ScreenLock = new ScreenLockService();
