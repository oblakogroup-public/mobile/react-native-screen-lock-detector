package com.screenlockdetector

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class ScreenReceiver(private val mainModule: ScreenLockDetectorModule): BroadcastReceiver() {
  private fun changeScreenState(isScreenOn: Boolean) {
    mainModule.isScreenOn = isScreenOn;
    mainModule.emitEvent("screenStateChanged", isScreenOn);
  }

  override fun onReceive(context: Context?, intent: Intent?) {
    intent ?: return

    when(intent.action) {
      Intent.ACTION_SCREEN_ON -> {
        Log.d(ScreenLockDetectorModule.NAME, "ACTION_SCREEN_ON")
        changeScreenState(true)
      }
      Intent.ACTION_SCREEN_OFF -> {
        Log.d(ScreenLockDetectorModule.NAME, "ACTION_SCREEN_OFF")
        changeScreenState(false)
      }
      else -> Unit
    }
  }
}
