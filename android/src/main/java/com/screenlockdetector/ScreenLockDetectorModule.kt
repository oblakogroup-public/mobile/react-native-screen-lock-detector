package com.screenlockdetector

import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.util.Log
import android.view.Display
import android.view.WindowManager
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter


class ScreenLockDetectorModule(private var reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext), LifecycleEventListener {
  private val screenReceiver: ScreenReceiver = ScreenReceiver(this)
  var isScreenOn = displayIs(Display.STATE_ON)

  override fun getName(): String {
    return NAME
  }

  @ReactMethod
  fun getDisplayState(promise: Promise) {
    promise.resolve(isScreenOn)
  }

  fun emitEvent(eventName: String, data: Any) {
    if (!reactContext.hasActiveReactInstance()) {
      return
    }

    reactContext
      .getJSModule(RCTDeviceEventEmitter::class.java)
      .emit(eventName, data)
  }

  @Suppress("Deprecation")
  private fun displayIs(state:Int): Boolean {
    val displayState = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
      reactContext.display?.state
    } else {
      val windowManager = reactContext.getSystemService(ReactApplicationContext.WINDOW_SERVICE) as? WindowManager
      windowManager?.defaultDisplay?.state
    }

    return state == displayState
  }

  init {
    val screenFilter = IntentFilter()

    screenFilter.addAction(Intent.ACTION_SCREEN_ON)
    screenFilter.addAction(Intent.ACTION_SCREEN_OFF)

    reactContext.registerReceiver(screenReceiver, screenFilter);
  }


  override fun onHostResume() {
    Log.d(NAME, "onHostResume")
  }

  override fun onHostPause() {
    Log.d(NAME, "onHostPause")
  }

  override fun onHostDestroy() {
    reactContext.unregisterReceiver(screenReceiver)
  }

  companion object {
    const val NAME = "ScreenLockDetector"
  }
}
