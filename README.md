# react-native-screen-lock-detector

Native module for detection lock screen

## Installation

```sh
npm install react-native-screen-lock-detector
```

## Usage

```ts
import { ScreenLock } from 'react-native-screen-lock-detector';

// ...

const isScreenOn = await ScreenLock.getDisplayState();

const subscription = ScreenLock.addListener('screenStateChanged', isScreenOn => {
  if (isScreenOn) {
    // ...
  } else {
    // ...
  }
})

// Remove subscription
ScreenLock.removeSubscription(subscription)
// or
subscription.remove()

// Remove all subscription from event
ScreenLock.removeAllListeners('onStateChanged')

```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)
